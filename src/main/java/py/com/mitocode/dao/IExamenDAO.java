package py.com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import py.com.mitocode.model.Examen;

@Repository
public interface IExamenDAO extends JpaRepository<Examen, Integer> {

}
