package py.com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import py.com.mitocode.model.Medico;

@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer> {

}
